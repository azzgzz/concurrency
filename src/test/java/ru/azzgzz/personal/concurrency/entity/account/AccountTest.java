package ru.azzgzz.personal.concurrency.entity.account;

import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AccountTest extends AbstractAccountTest {

    @Test
    @Order(1)
    void unsafeAccountTest() {
        oneWayTransferTest(UnsafeAccount::new);
        deadlockTest(UnsafeAccount::new);
    }

    @Test
    @Order(2)
    @Disabled
    void deadlockAccountTest() {
        oneWayTransferTest(DeadlockAccount::new);
        deadlockTest(DeadlockAccount::new);
    }

    @Test
    @Order(3)
    void synchronizedAccountTest() {
        oneWayTransferTest(SynchronizedAccount::new);
        deadlockTest(SynchronizedAccount::new);
    }

    @Test
    @Order(4)
    void lockAccountTest() {
        oneWayTransferTest(TryLockAccount::new);
        deadlockTest(TryLockAccount::new);
    }

    @Test
    @Order(5)
    void concurrencyAccountTest() {
        oneWayTransferTest(ConcurrencyAccount::new);
        deadlockTest(ConcurrencyAccount::new);
    }

}
