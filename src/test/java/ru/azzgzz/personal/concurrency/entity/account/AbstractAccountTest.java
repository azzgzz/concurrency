package ru.azzgzz.personal.concurrency.entity.account;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("rawtypes")
public abstract class AbstractAccountTest {

    public static final int INITIAL_AMOUNT = 1000000;

    void oneWayTransferTest(Supplier<Transferable> accountCreator) {
        Transferable account1 = accountCreator.get();
        Transferable account2 = accountCreator.get();
        Transferable account3 = accountCreator.get();

        account1.setInitialAmount(INITIAL_AMOUNT);

        testRun("One way transfer testing for " + account1.getClass().getSimpleName(), IntStream.range(0, 1000)
                .mapToObj(ignored -> new Runner(account1, account2, account3))
                .collect(Collectors.toList()));
    }

    void deadlockTest(Supplier<Transferable> accountCreator) {
        Transferable account1 = accountCreator.get();
        Transferable account2 = accountCreator.get();
        Transferable account3 = accountCreator.get();

        account1.setInitialAmount(INITIAL_AMOUNT);

        testRun("Deadlock transfer testing for " + account1.getClass().getSimpleName(), List.of(
                new Runner(account1, account2, account3),
                new Runner(account1, account2, account3),
                new Runner(account1, account3, account2),
                new Runner(account1, account3, account2)));

    }

    private void testRun(String testName, List<Runner> runners) {
        System.out.println(testName);

        long start = System.currentTimeMillis();
        List<Thread> threads = runners.stream()
                .map(Thread::new)
                .map(thread -> {
                    thread.start();
                    return thread;
                })
                .collect(Collectors.toList());

        threads.forEach(x -> {
            try {
                x.join(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Runner runner = runners.get(0);
        System.out.println("Account1 amount: " + runner.account1.getAmount());
        System.out.println("Account2 amount: " + runner.account2.getAmount());
        System.out.println("Account3 amount: " + runner.account3.getAmount());
        System.out.println("Extra total amount: " +
                (runner.account1.getAmount() + runner.account2.getAmount() + runner.account3.getAmount() - INITIAL_AMOUNT));
        long spentMillis = System.currentTimeMillis() - start;
        System.out.println("Total time spent: " + spentMillis);
        if (spentMillis > 4000) {
            System.out.println("DEADLOCKED");
        }
        System.out.println();
    }

    private static class Runner implements Runnable {

        private final Transferable account1;
        private final Transferable account2;
        private final Transferable account3;

        private Runner(Transferable account1, Transferable account2, Transferable account3) {
            this.account1 = account1;
            this.account2 = account2;
            this.account3 = account3;
        }

        @SuppressWarnings("unchecked")
        public void run() {
//            System.out.println("Thread " + Thread.currentThread().getName() + " started");
            for (int i = 0; i < INITIAL_AMOUNT ; i++) {
                account1.transfer(account2, 1);
                account2.transfer(account3, 1);
            }
        }
    }

}
