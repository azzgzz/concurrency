package ru.azzgzz.personal.concurrency.entity.account;

public interface Transferable<T extends Transferable<T>> {

    void transfer(T anotherAccount, long amount);

    long getAmount();

    void setInitialAmount(long initialAmount);
}
