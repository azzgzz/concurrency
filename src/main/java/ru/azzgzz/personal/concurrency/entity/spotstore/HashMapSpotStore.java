package ru.azzgzz.personal.concurrency.entity.spotstore;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class HashMapSpotStore implements SpotStore {

    private final ConcurrentHashMap<String, ConcurrentHashMap<LocalDateTime, Double>> spotStore;

    public HashMapSpotStore() {
        spotStore = new ConcurrentHashMap<>();
    }

    @Override
    public void add(String ccypair, double spot, LocalDateTime tickTime) {
        ConcurrentHashMap<LocalDateTime, Double> ccypairTimeline =
                spotStore.computeIfAbsent(ccypair, x -> new ConcurrentHashMap<>());
        ccypairTimeline.put(tickTime, spot);
    }

    @Override
    public double get(String ccypair, LocalDateTime dateTime) {
        return Optional.ofNullable(spotStore.get(ccypair))
                .map(ConcurrentHashMap::entrySet)
                .stream()
                .flatMap(Collection::stream)
                .filter(x -> !x.getKey().isAfter(dateTime))
                .max(java.util.Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .orElseThrow(() -> new IllegalArgumentException("Spot not found"));
    }

    @Override
    public void init(int days) {

    }
}
