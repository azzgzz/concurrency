package ru.azzgzz.personal.concurrency.entity.account;

import java.util.concurrent.atomic.AtomicLong;

public class ConcurrencyAccount implements Transferable<ConcurrencyAccount> {

    private final AtomicLong balance = new AtomicLong();

    public void transfer(ConcurrencyAccount anotherAccount, long amount) {
        if (amount <= 0 || balance.get() < amount) {
            return;
        }
        long current;
        do {
             current = balance.get();
        } while (amount <= current && !balance.compareAndSet(current, current - amount));
        do {
             current = anotherAccount.balance.get();
        } while (!anotherAccount.balance.compareAndSet(current, current + amount));
    }

    @Override
    public long getAmount() {
        return balance.get();
    }

    @Override
    public void setInitialAmount(long initialAmount) {
        balance.compareAndSet(balance.get(), initialAmount);
    }
}
