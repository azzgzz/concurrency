package ru.azzgzz.personal.concurrency.entity.account;

/**
 * Из-за положительного amount не требуется двухуровневая блокировка, в отличии от TryLockAccount.
 * Судя по тестам, отрабатывает быстрее
 *
 * One way transfer testing for SynchronizedAccount
 * Account1 amount: 0
 * Account2 amount: 0
 * Account3 amount: 100000
 * Extra total amount: 0
 * Total time spent: 32
 *
 * Deadlock transfer testing for SynchronizedAccount
 * Account1 amount: 0
 * Account2 amount: 49893
 * Account3 amount: 50107
 * Extra total amount: 0
 * Total time spent: 16
 */
public class SynchronizedAccount extends Account {

    @Override
    public void transfer(Account anotherAccount, long amount) {
        if (amount <= 0 || getAmount() < amount) {
            return;
        }

        synchronized (this) {
            if (getAmount() < amount) {
                return;
            }
            long newAmount = getAmount() - amount;
            setInitialAmount(newAmount);
        }
        synchronized (anotherAccount) {
            long newAnotherAmount = anotherAccount.getAmount() + amount;
            anotherAccount.setInitialAmount(newAnotherAmount);
        }
    }
}
