package ru.azzgzz.personal.concurrency.entity.spotstore;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Сложность метода add O(logN) - не подходит под условие
 */
public class NavigableSpotStore implements SpotStore {

    private final Map<String, ConcurrentSkipListMap<LocalDateTime, Double>> spotStore;

    public NavigableSpotStore() {
        this.spotStore = new ConcurrentHashMap<>();
    }

    @Override
    public void add(String ccypair, double spot, LocalDateTime tickTime) {
        ConcurrentSkipListMap<LocalDateTime, Double> ccypairTimeline =
                spotStore.computeIfAbsent(ccypair, x -> new ConcurrentSkipListMap<>());
        ccypairTimeline.put(tickTime, spot);
    }

    @Override
    public double get(String ccypair, LocalDateTime dateTime) {
        return Optional.ofNullable(spotStore.get(ccypair))
                .map(timeline -> timeline.floorEntry(dateTime))
                .map(Map.Entry::getValue)
                .orElseThrow(() -> new IllegalArgumentException("Spot not found"));
    }

    @Override
    public void init(int days) {

    }
}
