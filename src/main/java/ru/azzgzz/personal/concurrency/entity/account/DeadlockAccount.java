package ru.azzgzz.personal.concurrency.entity.account;

/**
 * One way transfer testing for DeadlockAccount
 * Account1 amount: 0
 * Account2 amount: 0
 * Account3 amount: 100000
 * Extra total amount: 0
 * Total time spent: 33
 *
 * Deadlock transfer testing for DeadlockAccount
 * Account1 amount: 99683
 * Account2 amount: 3
 * Account3 amount: 314
 * Extra total amount: 0
 * Total time spent: 4031
 * DEADLOCKED
 */
public class DeadlockAccount extends Account {
    @Override
    public void transfer(Account anotherAccount, long amount) {
        synchronized (this) {
            synchronized (anotherAccount) {
                swap(anotherAccount, amount);
            }
        }
    }
}
