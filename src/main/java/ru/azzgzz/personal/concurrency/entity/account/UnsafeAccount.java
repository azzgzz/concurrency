package ru.azzgzz.personal.concurrency.entity.account;

/**
 * Класс для проверки того, что юнит-тест выявляет гонку.
 *
 * One way transfer testing for UnsafeAccount
 * Account1 amount: 0
 * Account2 amount: 7843
 * Account3 amount: 103085
 * Extra total amount: 10928
 * Total time spent: 26
 *
 * Deadlock transfer testing for UnsafeAccount
 * Account1 amount: 0
 * Account2 amount: 52640
 * Account3 amount: 32338
 * Extra total amount: -15022
 * Total time spent: 11
 */
public class UnsafeAccount extends Account {

    public void transfer(Account anotherAccount, long amount) {
        swap(anotherAccount, amount);
    }

}
