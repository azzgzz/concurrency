package ru.azzgzz.personal.concurrency.entity.account;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * One way transfer testing for TryLockAccount
 * Account1 amount: 0
 * Account2 amount: 0
 * Account3 amount: 100000
 * Extra total amount: 0
 * Total time spent: 73
 *
 * Deadlock transfer testing for TryLockAccount
 * Account1 amount: 0
 * Account2 amount: 56663
 * Account3 amount: 43337
 * Extra total amount: 0
 * Total time spent: 83
 */
public class TryLockAccount extends Account {

    private final Lock lock = new ReentrantLock();

    @Override
    public void transfer(Account anotherAccount, long amount) {
        if (amount <= 0 || isBalanceNotEnough(amount)) {
            return;
        }
        if (!(anotherAccount instanceof TryLockAccount)) {
            throw new UnsupportedOperationException();
        }
        boolean lockSuccess = lock.tryLock();
        if (lockSuccess) {
            try {
                boolean secondLockSuccess = ((TryLockAccount) anotherAccount).lock.tryLock();
                if (secondLockSuccess) {
                    try {
                        swap(anotherAccount, amount);
                    } finally {
                        ((TryLockAccount) anotherAccount).lock.unlock();
                    }
                }
            } finally {
                lock.unlock();
            }
        }
    }

}
