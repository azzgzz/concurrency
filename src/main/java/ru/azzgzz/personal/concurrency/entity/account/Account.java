package ru.azzgzz.personal.concurrency.entity.account;

public abstract class Account implements Transferable<Account> {

    private long amount;

    public abstract void transfer(Account anotherAccount, long amount);

    public long getAmount() {
        return this.amount;
    }

    public void setInitialAmount(long amount) {
        this.amount = amount;
    }

    protected void swap(Account anotherAccount, long amount) {
        if (amount <= 0 || isBalanceNotEnough(amount))
            return;
        this.amount -= amount;
        anotherAccount.amount += amount;
    }

    protected boolean isBalanceNotEnough(long amount) {
        return this.amount < amount;
    }

}
